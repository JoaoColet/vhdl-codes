------------ top ---------------
library ieee;
use ieee.std_logic_1164.all;
use work.meu_pacote.all;

entity top is
	generic(tam : integer := 4;
			  freq : integer := 2);
	port( mclk, reset: in std_logic;
			selout: out std_logic_vector(3 downto 0);
			saida: out std_logic_vector(6 downto 0));
end entity;

architecture arq of top is
	signal result: std_logic_vector(tam-1 downto 0);
begin
	selout <= "1110";
	map_1: entity work.contador(arq)
		generic map(tam, freq)
		port map(mclk, reset, result);
	map_2: entity work.bcd_decod(arq)
		generic map(tam, freq)
		port map(result, saida);
end arq;

------------- contador -----------
library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
entity contador is
	generic(tam : integer; freq : integer);
	port(mclk, reset: in std_logic; clk: out std_logic_vector(tam-1 downto 0));
end entity;
architecture arq of contador is
	constant N: integer := 50_000_000;
	--constant N: integer := 8;
begin
	process(mclk, reset)
	variable i: integer range 1 to N := 1;
	variable v_clk: integer range 0 to 2**tam-1 := 0;
	begin
		if (reset = '1') then 
			i := 1; 
			v_clk := 0;
		elsif(mclk'event and mclk='1') then 
			i := i + 1;
			if (i = N/(2*freq)) then 
				i := 1; 
				v_clk := v_clk + 1;
			end if;
		clk <= std_logic_vector(to_unsigned(v_clk, tam));	
		end if;
	end process;
end arq;

--------- bcd_decod --------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity bcd_decod is
	generic(tam: integer; freq: integer);
	port( resultvec: IN std_logic_vector(tam-1 downto 0);
			segmentos: OUT std_logic_vector(6 DOWNTO 0));
end bcd_decod;

architecture arq of bcd_decod is
	signal result: integer range 0 to 15;
begin
	result <= to_integer(unsigned(resultvec));
segmentos <= "0000001" when result = 0 else -- '0'
				 "1001111" when result = 1 else -- '1'
				 "0010010" when result = 2 else -- '2'
				 "0000110" when result = 3 else -- '3'
				 "1001100" when result = 4 else -- '4'
				 "0100100" when result = 5 else -- '5'
				 "0100000" when result = 6 else -- '6'
				 "0001111" when result = 7 else -- '7'
				 "0000000" when result = 8 else -- '8'
				 "0000100" when result = 9 else -- '9'
				 "0001000" when result = 10 else -- 'A'
				 "1100000" when result = 11 else -- 'B'
				 "0110001" when result = 12 else -- 'C'
				 "1000010" when result = 13 else -- 'D'
				 "0110000" when result = 14 else -- 'E'
				 "0111000" when result = 15 else -- 'F'
				 "1111111";
end arq;

------------ tb ---------------
library ieee; use ieee.std_logic_1164.all;
entity tb_cont is end tb_cont;

architecture tb_arq of tb_cont is
	signal mclk, reset : std_logic := '0';
	signal selout: std_logic_vector(3 downto 0);
	signal clk : std_logic_vector(6 downto 0);
	constant mclk_period : time := 500 ms;
begin
	uut: entity work.top(arq)
	port map (mclk, reset, selout, clk);
	process begin
		mclk <= '0'; wait for mclk_period/2;
		mclk <= '1'; wait for mclk_period/2;
	end process;
	reset <= '1', '0' after mclk_period;
end;

----------- net ------------
net mclk loc=B8; #clk
net reset loc=G12; #btw0
net selout(3) loc=K14; #display4
net selout(2) loc=M13; #display3
net selout(1) loc=J12; #display2
net selout(0) loc=F12; #display1
net saida(6) loc=L14; #segA
net saida(5) loc=H12; #segB
net saida(4) loc=N14; #segC
net saida(3) loc=N11; #segD
net saida(2) loc=P12; #segE
net saida(1) loc=L13; #segF
net saida(0) loc=M12; #segG