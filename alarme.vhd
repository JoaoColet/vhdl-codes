----------------- top --------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.meu_pacote.all;

entity top is
	port( mclk, controle, sensor: in std_logic;
			status: inout std_logic;
			alarme: out std_logic);
end entity;

architecture arq of top is
begin 
	map_1: entity work.sts(arq)
		port map(mclk, controle, status);
	map_2: entity work.alarm(arq)
		port map(mclk, status, sensor, alarme);
end arq;

---------------- meu_pacote --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

package meu_pacote is
	component sts is
		port(mclk, controle: in std_logic;
			  status, aux: out std_logic);
	end component;
	
	component alarm is
		port(mclk, status, sensor: in std_logic;
				alarme: out std_logic);
	end component;
end meu_pacote;

------------------ status -------------------------
library ieee;
use ieee.std_logic_1164.all;

entity sts is
	port( clk, controle: in std_logic;
			status: out std_logic);
end sts;

architecture arq of sts is
	type state is (zero, wait_1, um, wait_0);
	signal pr_state: state := zero;
	signal nx_state: state;
	begin
	process(clk)
	begin
		if(clk'event and clk = '1') then pr_state <= nx_state;
		end if;
	end process;

	process(pr_state, controle)
	begin
	case pr_state is
		when zero =>
			status <= '0';
			if(controle = '1') then nx_state <= wait_1;
			else nx_state <= zero;
			end if;
 
		when wait_1 =>
			status <= '0';
			if(controle = '0') then nx_state <= um;
			else nx_state <= wait_1;
			end if;
 
		when um =>
			status <= '1';
			if(controle = '1') then nx_state <= wait_0;
			else nx_state <= um;
			end if;
			
		when wait_0 =>
			status <= '1';
			if(controle = '0') then nx_state <= zero;
			else nx_state <= wait_0;
			end if;
		end case;
	end process;
end arq;

--------------------- alarme --------------------------
library ieee;
use ieee.std_logic_1164.all;

entity alarm is
	port( clk, status, sensor: in std_logic;
			alarme: out std_logic);
end alarm;

architecture arq of alarm is
	type state is (zero, um, wait_alarme);
	signal pr_state: state := zero;
	signal nx_state: state;
	begin
	process(clk)
	begin
		if(clk'event and clk = '1') then pr_state <= nx_state;
		end if;
	end process;

	process(pr_state, status, sensor)
	begin
	case pr_state is
		when zero =>
			alarme <= '0';
			if(status = '1') then nx_state <= um;
			else nx_state <= zero;
			end if;
 
		when um =>
			alarme <= '0';
			if(status = '0') then nx_state <= zero;
			elsif(sensor = '1') then nx_state <= wait_alarme;
			else nx_state <= um;
			end if;
			
		when wait_alarme =>
			alarme <= '1';
			if(status = '0') then nx_state <= zero;
			elsif(sensor = '1') then nx_state <= wait_alarme;
			else nx_state <= um;
			end if;
			
		end case;
	end process;
end arq;

--------------------- net ---------------------------
net mclk loc = B8; #clock
net controle loc = C11; #btn1
net sensor loc = A7; #btn3
net status loc = G1; #ld7
net alarme loc = M11; #led1