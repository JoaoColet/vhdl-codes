--------- main ------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity subtrator_3b is
generic (N: integer :=3);
port(a, b : in std_logic_vector(N-1 downto 0);
		  y : out std_logic_vector(N downto 0));
end subtrator_3b;

architecture arq_subtrator_3b of subtrator_3b is
	signal ia, ib : integer range 0 to 2**N-1;
begin
		ia <= to_integer(unsigned(a));
		ib <= to_integer(unsigned(b));
		y  <= std_logic_vector(to_signed((ia-ib), N+1));
end arq_subtrator_3b;

-------- TestBranch ------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_subtrator_3b is
	generic(N : integer := 3);
end tb_subtrator_3b;

architecture arq_tb_subtrator_3b of tb_subtrator_3b is
	signal ta, tb : std_logic_vector(N-1 downto 0);
	signal     ty : std_logic_vector(N downto 0);

begin
	uut : entity work.subtrator_3b(arq_subtrator_3b)
			port map(ta, tb, ty);
			
	process
		variable i, j : integer range 0 to 2**N-1;
		
		begin
			for i in 0 to 2**N-1 loop
				for j in 0 to 2**N-1 loop
					ta <= std_logic_vector(to_unsigned(i, N));
					tb <= std_logic_vector(to_unsigned(j, N));
					wait for 10 ns; -- 640 ns no total
				end loop;
			end loop;
	end process;
end arq_tb_subtrator_3b;