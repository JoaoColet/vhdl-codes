------------- main -----------------
library ieee;
use ieee.std_logic_1164.all;
 
entity detectorseq is
	port(clk, rst: in std_logic;
					x: in std_logic_vector(2 downto 0);
					y: out std_logic);
end detectorseq;

architecture arq of detectorseq is
	constant N: integer := 50_000_000;
	type estado is (estado_A, estado_B, estado_C, estado_D);
	signal pr_st, nx_st: estado;
	begin
	--circuito sequencial--
	process(clk, rst)
	begin
		if(rst = '1') then pr_st <= estado_A;
		elsif(clk'event and clk = '1') then pr_st <= nx_st;
		end if;
	end process;
	---circuito combinacional---
	process(pr_st, x)
	begin
		case pr_st is
			when estado_A =>
				y <= '0';
				if(x = "001") then nx_st <= estado_B;
				else nx_st <= estado_A;
				end if;

			when estado_B =>
				y <= '0';
				if(x = "011") then nx_st <= estado_C;
				elsif(x = "001") then nx_st <= estado_B;
				else nx_st <= estado_A;
				end if;

			when estado_C =>
				y <= '0';
				if(x = "111") then nx_st <= estado_D;
				elsif(x = "011") then nx_st <= estado_C;
				elsif(x = "001") then nx_st <= estado_B;
				else nx_st <= estado_A;
				end if;

			when estado_D =>
				y <= '1';
				if(x = "111") then nx_st <= estado_D;
				elsif(x = "001") then nx_st <= estado_B;
				else nx_st <= estado_A;
				end if;
		end case;
	end process;
end arq;

---------------- net --------------
net x(2) LOC = K3;
net x(1) LOC = L3;
net x(0) LOC = P11;
net y LOC = M11;
net clk LOC = B8;
net rst LOC = A7;