---------- top.vhd ---------
library ieee;
use ieee.std_logic_1164.all;
use work.meu_pacote.all;
entity top is
generic(N: integer := 4);
port(a, b: in std_logic_vector(N-1 downto 0);
	  seldisp: out std_logic_vector(3 downto 0);
	  y: out std_logic_vector(7 downto 0));
end entity; 
architecture arq of top is 
	signal saida: std_logic_vector(N downto 0);
begin
	seldisp(3) <= '1';
	seldisp(2) <= '1';
	seldisp(1) <= '1';
	seldisp(0) <= '0';
	map_1: entity work.subtrator(arq)
			 generic map (N)
			 port map(a, b, saida);
	map_2: entity work.bcd_decod(arq)
			 generic map (N)
			 port map(saida, y);
end arq;

--------- meu_pacote.vhd -------------
library ieee;
use ieee.std_logic_1164.all;

package meu_pacote is

component subtrator is
	generic(N: integer);
	port(a, b: in std_logic_vector(N-1 downto 0);
		  y: out std_logic_vector(N downto 0));
end component;

component bcd_decod is
	generic(N: integer);
	port(bcd: in std_logic_vector(N downto 0);
		  segmentos: out std_logic_vector(7 downto 0));
end component;

end meu_pacote;

----------- subtrator.vhd ---------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity subtrator is
generic (N: integer);
port(a, b : in std_logic_vector(N-1 downto 0);
		  y : out std_logic_vector(N downto 0));
end subtrator;

architecture arq of subtrator is
	signal ia, ib : integer range 0 to 2**N-1;
begin
		ia <= to_integer(unsigned(a));
		ib <= to_integer(unsigned(b));
		y  <= std_logic_vector(to_signed((ia-ib), N+1));
end arq;

------------- bcd_decod.vhd -------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity bcd_decod is
generic(N: integer);
port(	bcd      : IN std_logic_vector(N downto 0);
		segmentos: OUT std_logic_vector(7 DOWNTO 0));
end bcd_decod;
 
architecture arq of bcd_decod is
	signal result: integer range -15 to 15;
begin
	result <= to_integer(signed(bcd));
	WITH result SELECT
		segmentos <= 	"00000010" when 0, -- '0'
							"10011110" when 1, -- '1'
							"00100100" when 2, -- '2'
							"00001100" when 3, -- '3'
							"10011000" when 4, -- '4'
							"01001000" when 5, -- '5'
							"01000000" when 6, -- '6'
							"00011110" when 7, -- '7'
							"00000000" when 8, -- '8'
							"00001000" when 9, -- '9'
							"00010000" when 10, -- 'A'
							"11000000" when 11, -- 'B'
							"01100010" when 12, -- 'C'
							"10000100" when 13, -- 'D'
							"01100000" when 14, -- 'E'
							"01110000" when 15, -- 'F'
							"10011111" when -1, -- '1.'
							"00100101" when -2, -- '2.'
							"00001101" when -3, -- '3.'
							"10011001" when -4, -- '4.'
							"01001001" when -5, -- '5.'
							"01000001" when -6, -- '6.'
							"00011111" when -7, -- '7.'
							"00000001" when -8, -- '8.'
							"00001001" when -9, -- '9.'
							"00010001" when -10, -- 'A.'
							"11000001" when -11, -- 'B.'
							"01100011" when -12, -- 'C.'
							"10000101" when -13, -- 'D.'
							"01100001" when -14, -- 'E.'
							"01110001" when -15, -- 'F.'
							"11111111" when others;
end arq;

--------------------- net.ucf -------------------
net a(3) loc=N3;   #SW7
net a(2) loc=E2;   #SW6
net a(1) loc=F3;   #SW5
net a(0) loc=G3;  #SW4
net b(3) loc=B4;   #SW3
net b(2) loc=K3;   #SW2
net b(1) loc=L3;   #SW1
net b(0) loc=P11;   #SW0

net selout(3) loc=K14; #display4
net selout(2) loc=M13; #display3
net selout(1) loc=J12; #display2
net selout(0) loc=F12; #display1

net result(7) loc=L14; #segA
net result(6) loc=H12; #segB
net result(5) loc=N14; #segC
net result(4) loc=N11; #segD
net result(3) loc=P12; #segE
net result(2) loc=L13; #segF
net result(1) loc=M12; #segG
net result(0) loc=N13; #segP

------------ tb.vhd --------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_subtrator is
	generic(N: integer := 4);
end tb_subtrator;

architecture arq_tb_subtrator of tb_subtrator is
	signal ta, tb : std_logic_vector(N-1 downto 0);
	signal tsel: std_logic_vector(3 downto 0);
	signal ty : std_logic_vector(7 downto 0);

begin
	uut : entity work.top(arq)
			port map(ta, tb, tsel, ty);
			
	process
		variable i, j : integer range 0 to 2**N-1;
		
		begin
			for i in 0 to 2**N-1 loop
				for j in 0 to 2**N-1 loop
					ta <= std_logic_vector(to_unsigned(i, N));
					tb <= std_logic_vector(to_unsigned(j, N));
					wait for 10 ns; 
				end loop;
			end loop;
	end process;
end arq_tb_subtrator;