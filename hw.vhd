----------- top.vhd ------------
library ieee;
use ieee.std_logic_1164.all;
use work.meu_pacote.all;
entity top is
generic(N: integer :=8);
port(a: in std_logic_vector(N-1 downto 0);
	  seldisp: out std_logic_vector(3 downto 0);
	  y: out std_logic_vector(6 downto 0));
end entity; 
architecture arq of top is 
	signal saida: integer range 0 to N;
begin
	seldisp(3) <= '1';
	seldisp(2) <= '1';
	seldisp(1) <= '1';
	seldisp(0) <= '0';
	map_1: entity work.hammingweight(arq)
			 generic map (N)
			 port map(a, saida);
	map_2: entity work.bcd_decod(arq)
			 port map(saida, y);
end arq;

---------- meu_pacote.vhd ----------------
library ieee;
use ieee.std_logic_1164.all;

package meu_pacote is

component hammingweight is
	generic(N: integer);
	port(a: in std_logic_vector(N-1 downto 0);
		  y: out integer range 0 to N);
end component;

component bcd_decod is
	port(bcd: in integer range 0 to 15;
		  segmentos: out std_logic_vector(6 downto 0));
end component;

end meu_pacote;

----------- hw.vhd  -----------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity hammingweight is
generic(N: integer);
port( a: in std_logic_vector(N-1 downto 0);
	  hw: out integer range 0 to N);
end hammingweight;

architecture arq of hammingweight is
	TYPE natural_array IS ARRAY (0 TO N) OF integer RANGE 0 TO N;
	SIGNAL internal: natural_array;
begin
	internal(0) <= 0;
	gen: FOR i IN 1 TO N GENERATE
		internal(i) <= internal(i-1) + 1 WHEN a(i-1)='1' ELSE internal(i-1);
	END GENERATE;
	hw <= internal(N);
end arq;

------ bcd_decod.vhd ------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity bcd_decod is
port(	bcd      : IN integer range 0 to 15;
		segmentos: OUT std_logic_vector(6 DOWNTO 0));
end bcd_decod;
 
architecture arq of bcd_decod is
begin
	WITH bcd SELECT
		segmentos <= 	"0000001" when 0, -- '0'
							"1001111" when 1, -- '1'
							"0010010" when 2, -- '2'
							"0000110" when 3, -- '3'
							"1001100" when 4, -- '4'
							"0100100" when 5, -- '5'
							"0100000" when 6, -- '6'
							"0001111" when 7, -- '7'
							"0000000" when 8, -- '8'
							"0000100" when 9, -- '9'
							"0001000" when 10, -- 'A'
							"1100000" when 11, -- 'B'
							"0110001" when 12, -- 'C'
							"1000010" when 13, -- 'D'
							"0110000" when 14, -- 'E'
							"0111000" when 15, -- 'F'
							"1111111" when others;
end arq;

-------- net.ucf ----------
net a(7) loc=N3;   #SW7
net a(6) loc=E2;   #SW6
net a(5) loc=F3;   #SW5
net a(4) loc=G3;  #SW4
net a(3) loc=B4;   #SW3
net a(2) loc=K3;   #SW2
net a(1) loc=L3;   #SW1
net a(0) loc=P11;   #SW0

net seldisp(3) loc=K14; #display4
net seldisp(2) loc=M13; #display3
net seldisp(1) loc=J12; #display2
net seldisp(0) loc=F12; #display1

net y(6) loc=L14; #segA
net y(5) loc=H12; #segB
net y(4) loc=N14; #segC
net y(3) loc=N11; #segD
net y(2) loc=P12; #segE
net y(1) loc=L13; #segF
net y(0) loc=M12; #segG

--------- tb.vhd ------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
ENTITY tb_hw IS
	generic(N: integer := 4);
END tb_hw;
 
ARCHITECTURE tb_arq OF tb_hw IS 
	signal ta: std_logic_vector(N-1 downto 0);
	signal tsd: std_logic_vector(3 downto 0);
	signal ty: std_logic_vector(6 downto 0);
begin
	uut: entity work.top(arq)
	port map(ta, tsd, ty);
	process
	variable i: integer :=0;
	begin
		for i in 0 to 2**N-1 loop
			ta <= std_logic_vector(to_unsigned(i,N));
			wait for 10 ns;
		end loop;
	end process;
END tb_arq;
