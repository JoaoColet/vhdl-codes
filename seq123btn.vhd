------------------- main --------------
library ieee;
use ieee.std_logic_1164.all;

entity detectorseqbtn is
	port( clk: in std_logic;
			rst: in std_logic;
			btn: in std_logic_vector(1 downto 0);
			led: out std_logic);
end detectorseqbtn;

architecture arq of detectorseqbtn is
	type state is (zero, wait_1, um, wait_2, dois, wait_3_1, wait_3_2, wait_3, tres);
	signal pr_state: state := zero;
	signal nx_state: state;
	begin
	process(clk, rst)
	begin
		if(rst = '1') then pr_state <= zero;
		elsif(clk'event and clk = '1') then pr_state <= nx_state;
		end if;
	end process;

	process(pr_state, btn)
	begin
	case pr_state is
		when zero =>
		led <= '0';
		if(btn = "01") then nx_state <= wait_1;
		else nx_state <= zero;
		end if;
 
		when wait_1 =>
			led <= '0';
			if(btn = "00") then nx_state <= um;
			elsif(btn = "01") then nx_state <= wait_1;
			else nx_state <= zero;
			end if;
 
		when um =>
			led <= '0';
			if(btn = "10") then nx_state <= wait_2;
			elsif(btn="00") then nx_state <= um;
			elsif(btn="01") then nx_state <= wait_1;
			else nx_state <= zero;
			end if;
			
		when wait_2 =>
			led <= '0';
			if(btn = "00") then nx_state <= dois;
			elsif(btn = "10") then nx_state <= wait_2;
			else nx_state <= zero;
			end if;
 
		when dois =>
			led <= '0';
			if(btn = "10") then nx_state <= wait_3_2;
			elsif(btn = "01") then nx_state <= wait_3_1;
			elsif(btn="00") then nx_state <= dois;
			else nx_state <= zero;
			end if;
 
		when wait_3_1 =>
			led <= '0';
			if(btn = "11") then nx_state <= wait_3;
			elsif(btn="01") then nx_state <= wait_3_1;
			elsif(btn="00") then nx_state <= um;
			else nx_state <= zero;
			end if;
			
		when wait_3_2 =>
			led <= '0';
			if(btn = "11") then nx_state <= wait_3;
			elsif(btn="10") then nx_state <= wait_3_2;
			else nx_state <= zero;
			end if;
	
		when wait_3 =>
			led <= '0';
			if(btn = "11" or btn = "01" or btn = "10") then nx_state <= wait_3;
			elsif(btn = "00") then nx_state <= tres;
			else nx_state <= zero;
			end if;
		
		when tres =>
			led <= '1';
			if(btn = "00") then nx_state <= tres;
			elsif(btn = "01") then nx_state <= wait_1;
			else nx_state <= zero;
			end if;
		end case;
	end process;
end arq;

-------------------- net ---------------
NET "clk" LOC = B8; #CLK
NET "rst" LOC = M4; #BTN2
NET "btn(1)" LOC = C11; #BTN1
NET "btn(0)" LOC = G12; #BTN0
NET "led" LOC = M11; #LD1
