----- TOP.vhd --------
library ieee;
use ieee.std_logic_1164.all;
use work.meu_pacote.all;
entity top is
generic(N: integer :=3);
port(a, b: in std_logic_vector(N-1 downto 0);
	  selin: in std_logic; 
	  selout: out std_logic_vector(3 downto 0); 
	  result: out std_logic_vector(6 downto 0));
end entity; 
architecture arq of top is 
	signal saidaq, saidar: integer range 0 to 2**N-1;
begin
	selout(3) <= '1';
	selout(2) <= '1';
	selout(1) <= '1';
	selout(0) <= '0';
	map_1: entity work.divisor3bits(arq)
			 generic map(N)
			 port map(a, b, saidaq, saidar);
	map_2: entity work.bcd_decod(arq)
			 port map(saidaq, saidar, selin, result);
end arq; 

------ meu_pacote.vhd -----------
library ieee;
use ieee.std_logic_1164.all;

package meu_pacote is
component hammingweight is
	generic(N: integer);
	port(a, b: in std_logic_vector(N-1 downto 0);
		  saidaq, saidar: out integer range 0 to 2**N-1);
end component;

component bcd_decod is
	generic(N: integer);
	port(saidaq, saidar: in integer range 0 to 2**N-1;
		  selin: in std_logic;
		  result: out std_logic_vector(6 downto 0));
end component;

end meu_pacote;

--------- divisor3bits.vhd  ----------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity divisor3bits is
generic(N: integer);
port( a, b: in std_logic_vector(N-1 downto 0);
		q, r: out integer range 0 to 2**N-1);
end divisor3bits;

architecture arq of divisor3bits is
	TYPE natural_array IS ARRAY (0 TO 2**N-1) OF integer RANGE 0 TO 2**N-1;
	SIGNAL internalq, internalr: natural_array;
	signal sa, sb: integer range 0 to 2**N-1;
begin
	sa <= to_integer(unsigned(a));
	sb <= to_integer(unsigned(b));
	
	internalq(0) <= 2**N-1 when sb=0 else 0;
	internalr(0) <= 2**N-1 when sb=0 else 0;
	gen: FOR i IN 1 TO 2**N-1 GENERATE
		internalq(i) <= i WHEN (sa >= (i*sb) and (sa < (i+1)*sb)) ELSE internalq(i-1);
		internalr(i) <= i when (sa-(internalq(2**N-1)*sb)) = i and (sb/=0) else internalr(i-1);
	END GENERATE;
	q <= internalq(2**N-1);
	r <= internalr(2**N-1);
end arq;

----------- bcd_decod.vhd ------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity bcd_decod is
port(	bcdq, bcdr: IN integer range 0 to 15;
		bcdsel: in std_logic;
		segmentos: OUT std_logic_vector(6 DOWNTO 0));
end bcd_decod;
 
architecture arq of bcd_decod is
begin
	segmentos <= "0000001" when bcdq = 0 and bcdsel = '1' else -- '0'
					 "1001111" when bcdq = 1 and bcdsel = '1' else -- '1'
					 "0010010" when bcdq = 2 and bcdsel = '1' else -- '2'
			   	 "0000110" when bcdq = 3 and bcdsel = '1' else -- '3'
					 "1001100" when bcdq = 4 and bcdsel = '1' else -- '4'
					 "0100100" when bcdq = 5 and bcdsel = '1' else -- '5'
					 "0100000" when bcdq = 6 and bcdsel = '1' else -- '6'
					 "0001111" when bcdq = 7 and bcdsel = '1' else -- '7'
					 "0000000" when bcdq = 8 and bcdsel = '1' else -- '8'
					 "0000100" when bcdq = 9 and bcdsel = '1' else -- '9'
					 "0001000" when bcdq = 10 and bcdsel = '1' else -- 'A'
					 "1100000" when bcdq = 11 and bcdsel = '1' else -- 'B'
					 "0110001" when bcdq = 12 and bcdsel = '1' else -- 'C'
					 "1000010" when bcdq = 13 and bcdsel = '1' else -- 'D'
					 "0110000" when bcdq = 14 and bcdsel = '1' else -- 'E'
					 "0111000" when bcdq = 15 and bcdsel = '1' else -- 'F'
					 "0000001" when bcdr = 0 and bcdsel = '0' else -- '0'
					 "1001111" when bcdr = 1 and bcdsel = '0' else -- '1'
					 "0010010" when bcdr = 2 and bcdsel = '0' else -- '2'
					 "0000110" when bcdr = 3 and bcdsel = '0' else -- '3'
					 "1001100" when bcdr = 4 and bcdsel = '0' else -- '4'
					 "0100100" when bcdr = 5 and bcdsel = '0' else -- '5'
					 "0100000" when bcdr = 6 and bcdsel = '0' else -- '6'
					 "0001111" when bcdr = 7 and bcdsel = '0' else -- '7'
					 "0000000" when bcdr = 8 and bcdsel = '0' else -- '8'
					 "0000100" when bcdr = 9 and bcdsel = '0' else -- '9'
					 "0001000" when bcdr = 10 and bcdsel = '0' else -- 'A'
					 "1100000" when bcdr = 11 and bcdsel = '0' else -- 'B'
					 "0110001" when bcdr = 12 and bcdsel = '0' else -- 'C'
					 "1000010" when bcdr = 13 and bcdsel = '0' else -- 'D'
					 "0110000" when bcdr = 14 and bcdsel = '0' else -- 'E'
					 "0111000" when bcdr = 15 and bcdsel = '0' else -- 'F'
					 "1111111";
end arq;


------------ tb_div.vhd  -----------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
ENTITY tb_divisor3bits IS
	generic(N: integer :=3);
END tb_divisor3bits;
 
ARCHITECTURE tb_arq OF tb_divisor3bits IS 
	signal ta, tb: std_logic_vector(N-1 downto 0);
	signal tselin: std_logic; 
	signal tselout: std_logic_vector(3 downto 0); 
	signal trresult: std_logic_vector(6 downto 0);
begin
	uut: entity work.top(arq)
	port map(ta, tb, tselin, tselout, tresult);
	process
	variable i, j: integer :=0;
	begin
		for i in 0 to 2**N-1 loop
			for j in 0 to 2**N-1 loop
				ta <= std_logic_vector(to_unsigned(i, N));
				tb <= std_logic_vector(to_unsigned(j, N));
				wait for 10 ns;
			end loop;
		end loop;
	end process;
END tb_arq;

----- net.ucf -----------
net a(2) loc=N3;   #SW7
net a(1) loc=E2;   #SW6
net a(0) loc=F3;   #SW5
net b(2) loc=G3;  #SW4
net b(1) loc=B4;   #SW3
net b(0) loc=K3;   #SW2
#net a(1) loc=L3;   #SW1
net selin loc=P11;   #SW0

net selout(3) loc=K14; #display4
net selout(2) loc=M13; #display3
net selout(1) loc=J12; #display2
net selout(0) loc=F12; #display1

net result(6) loc=L14; #segA
net result(5) loc=H12; #segB
net result(4) loc=N14; #segC
net result(3) loc=N11; #segD
net result(2) loc=P12; #segE
net result(1) loc=L13; #segF
net result(0) loc=M12; #segG