------- contador  ---------------
library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
entity contador2Hz is
	generic(freq : integer := 2; tam : integer := 4);
	port(mclk, reset: in std_logic; clk: out std_logic_vector(tam-1 downto 0));
end entity;
architecture arq of contador2Hz is
	constant N: integer := 50_000_000;
	--constant N: integer := 8;
begin
	process(mclk, reset)
	variable i: integer range 1 to N := 1;
	variable v_clk: integer range 0 to 2**tam-1 := 0;
	begin
		if (reset = '1') then 
			i := 1; 
			v_clk := 0;
		elsif(mclk'event and mclk='1') then 
			i := i + 1;
			if (i = N/(2*freq)) then 
				i := 1; 
				v_clk := v_clk + 1;
			end if;
		clk <= std_logic_vector(to_unsigned(v_clk, tam));	
		end if;
	end process;
end arq;

------ tb -------------
library ieee; use ieee.std_logic_1164.all;
entity tb_piscaled is end tb_piscaled;

architecture tb_arq of tb_piscaled is
	signal mclk, reset : std_logic := '0';
	signal clk : std_logic_vector(3 downto 0);
	constant mclk_period : time := 500 ms;
begin
	uut: entity work.contador2Hz(arq)
	port map (mclk, reset, clk);
	process begin
		mclk <= '0'; wait for mclk_period/2;
		mclk <= '1'; wait for mclk_period/2;
	end process;
	reset <= '1', '0' after mclk_period;
end;

----------- net -------------
net clk(3) loc=N5; #LED4
net clk(2) loc=P6; #LED3
net clk(1) loc=P7; #LED2
net clk(0) loc=M11; #LED1
net reset loc=G12; #B0
net mclk loc=B8; #clock