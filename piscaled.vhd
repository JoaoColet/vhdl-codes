------------- main ----------
library ieee; use ieee.std_logic_1164.all;
entity piscaled is
	port(mclk, reset: in std_logic; clk1, clk2: out std_logic);
end entity;
architecture arq of piscaled is
	constant N: integer := 50_000_000;
	--constant N: integer := 8;
begin
	process(mclk, reset)
	variable i, j: integer range 1 to N := 1;
	variable v_clk1, v_clk2: std_logic := '0';
	begin
		if (reset = '1') then 
			i := 1; 
			v_clk1 := '0';
			v_clk2 := '0';
		elsif(mclk'event and mclk='1') then 
			i := i + 1;
			if (i = N/4) then 
				i := 1; 
				v_clk1 := not(v_clk1);
				j := j+1;
			end if;
			if (j = 3) then 
				j := 1; 
				v_clk2 := not(v_clk2);
			end if;
		clk1 <= v_clk1;
		clk2 <= v_clk2;	
		end if;
	end process;
end arq;

----------- tb -------------
library ieee; use ieee.std_logic_1164.all;
entity tb_piscaled is end tb_piscaled;

architecture tb_arq of tb_piscaled is
	signal mclk, reset, clk1, clk2 : std_logic := '0';
	constant mclk_period : time := 250 ms;
begin
	uut: entity work.piscaled(arq)
	port map (mclk, reset, clk1, clk2);
	process begin
		mclk <= '0'; wait for mclk_period/2;
		mclk <= '1'; wait for mclk_period/2;
	end process;
	reset <= '1', '0' after mclk_period;
end;

------------- net ---------------
net clk1 loc=M11; #LED1
net clk2 loc=P7; #LED2
net reset loc=G12; #BTN0
net mclk loc=B8; #CLOCK